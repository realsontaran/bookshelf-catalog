import 'package:flutter/material.dart';

const backgroundColor = Color(0xff202125);
const primaryColor = Color(0xff40AFFF);
const textColor = Color(0xffCECECE);

class MyAppTheme {
  static final ThemeData darkTheme = ThemeData(
    brightness: Brightness.dark,
    primaryColor: primaryColor,
    scaffoldBackgroundColor: backgroundColor,
    fontFamily: 'Ubuntu',
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
      elevation: 10,
      backgroundColor: backgroundColor,
      selectedItemColor: primaryColor,
      unselectedItemColor: Color(0xff9D9D9D),
    ),
    textTheme: const TextTheme(
      displayLarge: TextStyle(
          color: Colors.white, fontSize: 24.0, fontWeight: FontWeight.bold),
      displayMedium: TextStyle(
          color: textColor, fontSize: 18.0, fontWeight: FontWeight.normal),
      bodyLarge: TextStyle(color: textColor, fontSize: 18.0),
      bodyMedium: TextStyle(color: textColor, fontSize: 14.0),
    ),
    buttonTheme: ButtonThemeData(
      buttonColor: const Color(0xff1da0ff),
      textTheme: ButtonTextTheme.primary,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
    ),
    cardTheme: CardTheme(
      elevation: 5.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      contentPadding: const EdgeInsets.only(
          left: 69, top: 200), // Adjust the top padding value as needed
      fillColor: const Color(0xFF303135),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
        borderSide: BorderSide.none,
      ),
    ),
  );

  static const searchBoxDecoration = InputDecoration(
    hintText: 'Search Book',
    hintStyle: TextStyle(
      fontFamily: 'Ubuntu',
      fontWeight: FontWeight.w400,
      fontSize: 18,
      color: Color(0xFF999CA1),
    ),
    prefixIcon: Icon(
      Icons.search,
      size: 30,
      color: Colors.white,
    ),
  );
}
