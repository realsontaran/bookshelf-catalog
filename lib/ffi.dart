import 'dart:ffi';
import 'dart:io';

import 'package:ffi/ffi.dart';

DynamicLibrary _lib = Platform.isAndroid
    ? DynamicLibrary.open('libOpenCV_ffi.so')
    : DynamicLibrary.process();

final _drawSpineLines = _lib
    .lookup<NativeFunction<Int32 Function(Pointer<Utf8>, Pointer<Utf8>)>>(
        'draw_spine_lines')
    .asFunction<int Function(Pointer<Utf8>, Pointer<Utf8>)>();

int drawSpineLines(String inputPath, String outputPath) {
  return _drawSpineLines(inputPath.toNativeUtf8(), outputPath.toNativeUtf8());
}

final _featureFinder = _lib
    .lookup<
        NativeFunction<
            Int32 Function(
                Pointer<Utf8>, Pointer<Utf8>, Pointer<Utf8>)>>('feature_finder')
    .asFunction<int Function(Pointer<Utf8>, Pointer<Utf8>, Pointer<Utf8>)>();

int featureFounder(String? objPath, String? scenePath) {
  return _featureFinder(objPath!.toNativeUtf8(), scenePath!.toNativeUtf8(),
      scenePath.toNativeUtf8());
}
