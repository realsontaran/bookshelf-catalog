class Book {
  final String title;
  final List<String> authors;
  final String imageUrl;
  final double rating;
  final String description;

  Book({
    required this.title,
    required this.authors,
    required this.imageUrl,
    required this.rating,
    required this.description,
  });
}
