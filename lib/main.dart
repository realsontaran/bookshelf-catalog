import 'package:bookshelf/widgets/bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'blocs/image_paths_cubit.dart';
import 'screens/camera_gallery_page.dart';
import 'screens/home_page.dart';
import 'screens/library_page.dart';
import 'blocs/tab_bloc.dart';
import 'blocs/search_bloc.dart';

import 'theme/custom_theme.dart';

void main() {
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<TabBloc>(
          create: (context) => TabBloc(),
        ),
        BlocProvider<SearchBloc>(
          create: (context) => SearchBloc(),
        ),
        BlocProvider<ImagePathsCubit>(
          create: (context) => ImagePathsCubit(),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: MyAppTheme.darkTheme,
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final tabBloc = BlocProvider.of<TabBloc>(context);
    return BlocBuilder<TabBloc, int>(
      bloc: tabBloc,
      builder: (context, currentIndex) {
        return Scaffold(
          body: IndexedStack(
            index: currentIndex,
            children: const [
              HomePage(),
              CameraGalleryPage(),
              LibraryPage(),
            ],
          ),
          bottomNavigationBar: BottomNavBar(tabBloc: tabBloc),
        );
      },
    );
  }
}
