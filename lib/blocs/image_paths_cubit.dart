import 'package:flutter_bloc/flutter_bloc.dart';

class ImagePathsCubit extends Cubit<List<String>?> {
  ImagePathsCubit() : super(null);

  void setImagePaths(List<String>? imagePaths) {
    emit(imagePaths);
  }
}
