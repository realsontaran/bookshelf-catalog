import 'package:flutter_bloc/flutter_bloc.dart';

enum SearchEvent {
  searchStarted,
  searchTextChanged,
  searchCompleted,
}

class SearchBloc extends Bloc<SearchEvent, String> {
  SearchBloc() : super('') {
    on<SearchEvent>(
      (event, emit) {
        switch (event) {
          case SearchEvent.searchStarted:
            // Perform any initialization logic here
            break;
          case SearchEvent.searchTextChanged:
            // Perform search text processing or filtering logic here
            // For simplicity, let's assume the new text is directly emitted as state
            emit(state);
            break;
          case SearchEvent.searchCompleted:
            // Perform any cleanup or finalization logic here
            break;
        }
      },
    );
  }

  void startSearch() {
    add(SearchEvent.searchStarted);
  }

  void updateSearchText(String searchText) {
    add(SearchEvent.searchTextChanged);
  }

  void completeSearch() {
    add(SearchEvent.searchCompleted);
  }
}
