import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../models/book_model.dart';
import 'database_helper.dart';

class BookDetailsDialog extends StatelessWidget {
  final Book book;
  const BookDetailsDialog({super.key, required this.book});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.zero,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      content: SizedBox(
        width: 400, // Specify the desired width
        height: 450, // Specify the desired height
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.network(
                      book.imageUrl,
                      fit: BoxFit.cover,
                      height: 200,
                      width: 150,
                      errorBuilder: (context, error, stackTrace) {
                        return Container(
                          color: Colors.grey[300],
                          child: Center(
                            child: Icon(
                              Icons.broken_image,
                              color: Colors.grey[600],
                              size: 64,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Title:${book.title}',
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(height: 24),
                          const Text(
                            'Authors:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            book.authors.join(', '),
                            style:
                                const TextStyle(fontWeight: FontWeight.normal),
                          ),
                          const SizedBox(height: 16),
                          const Text(
                            'Rating:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          RatingBar.builder(
                            onRatingUpdate: (rating) {},
                            initialRating: book.rating,
                            minRating: 0,
                            maxRating: 5,
                            itemSize: 18,
                            ignoreGestures: true,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemPadding:
                                const EdgeInsets.symmetric(horizontal: 2.0),
                            itemBuilder: (context, _) => const Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Description:',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 8),
                    Text(book.description),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text('Close'),
        ),
        TextButton(
          onPressed: () {
            _addToWishlist();
          },
          child: const Text('Wishlist'),
        ),
      ],
    );
  }

  Future<void> _addToWishlist() async {
    final Map<String, dynamic> bookData = {
      'title': book.title,
      'authors': book.authors.join(', '),
      'imageUrl': book.imageUrl,
      'rating': book.rating,
      'description': book.description,
    };

    await DatabaseHelper.instance.addToWishlist(bookData);

    Fluttertoast.showToast(
        msg: "Wishlisted ${book.title}",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
