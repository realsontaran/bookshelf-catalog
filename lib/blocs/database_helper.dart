import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  static Database? _database;

  DatabaseHelper._privateConstructor();

  Future<Database> get database async {
    if (_database != null) {
      return _database!;
    }

    _database = await _initDatabase();
    return _database!;
  }

  Future<Database> _initDatabase() async {
    final String path = join(await getDatabasesPath(), 'wishlist.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _createDatabase,
    );
  }

  Future<void> _createDatabase(Database db, int version) async {
    await db.execute('''
      CREATE TABLE wishlist (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT,
        authors TEXT,
        imageUrl TEXT,
        rating REAL,
        description TEXT
      )
      ''');
  }

  Future<List<Map<String, dynamic>>> getWishlist() async {
    final Database db = await database;
    return await db.query('wishlist');
  }

  Future<int> addToWishlist(Map<String, dynamic> bookData) async {
    final Database db = await database;
    return await db.insert('wishlist', bookData);
  }

  Future<int> removeFromWishlist(int id) async {
    final Database db = await database;
    return await db.delete(
      'wishlist',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
