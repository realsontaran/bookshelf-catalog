import 'package:flutter_bloc/flutter_bloc.dart';
import 'constants/nav_bar_items.dart';

class TabBloc extends Bloc<TabEvent, int> {
  TabBloc() : super(0) {
    on<TabEvent>(
      (event, emit) {
        switch (event) {
          case TabEvent.home:
            emit(0);
            break;
          case TabEvent.cameraGallery:
            emit(1);
            break;
          case TabEvent.library:
            emit(2);
            break;
        }
      },
    );
  }

  void changeTab(int index) {
    switch (index) {
      case 0:
        add(TabEvent.home);
        break;
      case 1:
        add(TabEvent.cameraGallery);
        break;
      case 2:
        add(TabEvent.library);
        break;
    }
  }
}
