import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../models/book_model.dart';

class DataSearch extends SearchDelegate<String> {
  Future<List<Book>> searchBooks(String query) async {
    final url =
        'https://www.googleapis.com/books/v1/volumes?q=$query&maxResults=20&orderBy=relevance';
    final response = await http.get(Uri.parse(url));
    const defaultImageUrl =
        "https://onlinebookclub.org/book-covers/id467907-125.jpg";

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final bookItems = data['items'] as List<dynamic>;

      return bookItems
          .map(
            (item) => Book(
              title: item['volumeInfo']['title'] ?? 'Unknown Title',
              authors: List<String>.from(item['volumeInfo']['authors'] ?? []),
              imageUrl: item['volumeInfo']['imageLinks']?['thumbnail'] ??
                  defaultImageUrl,
              rating: item['volumeInfo']['averageRating']?.toDouble() ?? 0.0,
              description: item['volumeInfo']['description'] ?? '',
            ),
          )
          .toList();
    } else {
      throw Exception('Failed to load books');
    }
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, "");
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return buildSuggestions(context);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isEmpty) {
      return const Center(child: Text('Enter a search term'));
    } else {
      return FutureBuilder<List<Book>>(
        future: searchBooks(query),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final books = snapshot.data;
            return ListView.builder(
              itemCount: books?.length,
              itemBuilder: (context, index) => ListTile(
                onTap: () {
                  close(context, books[index].title);
                },
                title: Text(books![index].title),
                subtitle: Text(books[index].authors.join(', ')),
              ),
            );
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    }
  }
}
