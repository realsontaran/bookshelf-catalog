import 'package:bookshelf/theme/custom_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bookshelf/blocs/search_bloc.dart';

class MySearchBar extends StatelessWidget {
  const MySearchBar({Key? key, required this.searchBloc}) : super(key: key);
  final SearchBloc searchBloc;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, String>(
      bloc: searchBloc,
      builder: (context, searchString) {
        return AppBar(
          title: TextField(
            onTap: () {
              context.read<SearchBloc>().startSearch();
            },
            onChanged: (text) {
              context.read<SearchBloc>().updateSearchText(text);
            },
            decoration: MyAppTheme.searchBoxDecoration,
          ),
        );
      },
    );
  }
}
