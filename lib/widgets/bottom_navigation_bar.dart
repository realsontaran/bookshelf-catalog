import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bookshelf/blocs/tab_bloc.dart';

class BottomNavBar extends StatelessWidget {
  const BottomNavBar({Key? key, required this.tabBloc}) : super(key: key);
  final TabBloc tabBloc;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TabBloc, int>(
      bloc: tabBloc,
      builder: (context, tabIndex) {
        return BottomNavigationBar(
          currentIndex: tabIndex,
          onTap: (index) => context.read<TabBloc>().changeTab(index),
          type: BottomNavigationBarType.fixed, // Shifting
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.photo_camera),
              label: 'Camera/Gallery',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.library_books),
              label: 'Library',
            ),
          ],
        );
      },
    );
  }
}
