import 'package:bookshelf/blocs/image_paths_cubit.dart';
import 'package:bookshelf/screens/ocr_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:io';
import '../blocs/data_search.dart';
import '../models/book_model.dart';
import 'image_pick.dart';
import 'wishlist_screen.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class LibraryPage extends StatefulWidget {
  const LibraryPage({Key? key}) : super(key: key);

  @override
  _LibraryPageState createState() => _LibraryPageState();
}

class _LibraryPageState extends State<LibraryPage> {
  List<Book> filteredBooks = [];
  List<ValueNotifier<String>> titles = [];

  Future<void> searchBooks(String query) async {
    final url =
        'https://www.googleapis.com/books/v1/volumes?q=$query&maxResults=20&orderBy=relevance';
    final response = await http.get(Uri.parse(url));
    const defaultImageUrl =
        "https://onlinebookclub.org/book-covers/id467907-125.jpg";

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final bookItems = data['items'] as List<dynamic>;

      final books = bookItems
          .map(
            (item) => Book(
              title: item['volumeInfo']['title'] ?? 'Unknown Title',
              authors: List<String>.from(item['volumeInfo']['authors'] ?? []),
              imageUrl: item['volumeInfo']['imageLinks']?['thumbnail'] ??
                  defaultImageUrl,
              rating: item['volumeInfo']['averageRating']?.toDouble() ?? 0.0,
              description: item['volumeInfo']['description'] ?? '',
            ),
          )
          .toList();

      setState(() {
        filteredBooks = books;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    final imagePaths = context.read<ImagePathsCubit>().state;

    if (!(imagePaths == null || imagePaths.isEmpty)) {
      titles = List<ValueNotifier<String>>.generate(
        imagePaths.length,
        (index) => ValueNotifier<String>("Book $index"),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final imagePaths = context.watch<ImagePathsCubit>().state;

    if (!(imagePaths == null || imagePaths.isEmpty)) {
      titles = List<ValueNotifier<String>>.generate(
        imagePaths.length,
        (index) => ValueNotifier<String>("Book $index"),
      );
    }

    return Scaffold(
      body: Stack(
        children: [
          if (imagePaths == null || imagePaths.isEmpty)
            const Center(
              child: Text("You don't have any books yet"),
            )
          else
            ListView.builder(
              itemCount: imagePaths.length,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () async {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: const Text('Choose an action'),
                          content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              // Display the image
                              Image.file(
                                height: 250,
                                File(imagePaths[index]),
                              ),
                            ],
                          ),
                          actions: <Widget>[
                            TextButton(
                              child: const Text('Set Name of the Book'),
                              onPressed: () async {
                                Navigator.of(context).pop();
                                final selected = await showSearch(
                                  context: context,
                                  delegate: DataSearch(),
                                );
                                if (selected != null) {
                                  titles[index].value = selected;
                                }
                              },
                            ),
                            TextButton(
                              child: const Text('Search in Image'),
                              onPressed: () {
                                Navigator.of(context).pop();
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => ImagePickPage(
                                      spinePath: imagePaths[index],
                                    ),
                                  ),
                                );
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                  child: ValueListenableBuilder<String>(
                    valueListenable: titles[index],
                    builder: (context, value, child) {
                      return ListTile(
                        leading: Image.file(File(imagePaths[index]),
                            width: 50, height: 50),
                        title: Text(value),
                      );
                    },
                  ),
                );
              },
            ),
          Positioned(
            top: 40.0,
            right: 120.0,
            child: MaterialButton(
              color: Colors.blue,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const OCRScreen()),
                );
              },
              child: const Icon(Icons.search),
            ),
          ),
          Positioned(
            top: 40.0,
            right: 20.0,
            child: MaterialButton(
              color: Colors.blue,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const WishlistScreen()),
                );
              },
              child: const Icon(Icons.star),
            ),
          ),
        ],
      ),
    );
  }
}
