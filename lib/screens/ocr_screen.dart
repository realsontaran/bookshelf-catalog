import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_tesseract_ocr/flutter_tesseract_ocr.dart';
import 'package:image_picker/image_picker.dart';

class OCRScreen extends StatefulWidget {
  const OCRScreen({super.key});

  @override
  _OCRScreenState createState() => _OCRScreenState();
}

class _OCRScreenState extends State<OCRScreen> {
  File? _pickedImage;
  String _recognizedText = '';

  Future<void> _pickImage(ImageSource source) async {
    final pickedImage =
        await ImagePicker().pickImage(source: source, imageQuality: 100);

    if (pickedImage != null) {
      setState(() {
        _pickedImage = File(pickedImage.path);
        _recognizedText = '';
      });
    }
  }

  Future<void> _performOCR() async {
    if (_pickedImage != null) {
      try {
        final text = await FlutterTesseractOcr.extractText(
          _pickedImage!.path,
          language: "eng+tur",
        );
        setState(() {
          _recognizedText = text;
        });
      } catch (e) {
        setState(() {
          _recognizedText = 'Error: $e';
        });
      }
    } else {
      setState(() {
        _recognizedText = 'No image selected';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Book OCR'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ElevatedButton(
                onPressed: () => _pickImage(ImageSource.camera),
                child: const Text('Take a Photo of Book'),
              ),
              const SizedBox(height: 16.0),
              if (_pickedImage != null) Image.file(_pickedImage!),
              const SizedBox(height: 16.0),
              ElevatedButton(
                onPressed: _performOCR,
                child: const Text('Detect Title'),
              ),
              const SizedBox(height: 16.0),
              const Text(
                'Recognized Text:',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 8.0),
              Text(_recognizedText),
            ],
          ),
        ),
      ),
    );
  }
}
