import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class WishlistScreen extends StatefulWidget {
  const WishlistScreen({super.key});

  @override
  _WishlistScreenState createState() => _WishlistScreenState();
}

class _WishlistScreenState extends State<WishlistScreen> {
  List<Map<String, dynamic>> wishlistItems = [];

  @override
  void initState() {
    super.initState();
    _fetchWishlistItems();
  }

  Future<void> _fetchWishlistItems() async {
    final Database db = await openDatabase(
      join(await getDatabasesPath(), 'wishlist.db'),
    );

    final List<Map<String, dynamic>> items = await db.query('wishlist');

    setState(() {
      wishlistItems = items;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: wishlistItems.length,
        itemBuilder: (context, index) {
          final item = wishlistItems[index];
          return ListTile(
            leading: Image.network(item['imageUrl'] ??
                "https://onlinebookclub.org/book-covers/id467907-125.jpg"),
            title: Text(item['title']),
            subtitle: Text(item['authors']),
            trailing: IconButton(
              icon: const Icon(Icons.delete),
              onPressed: () async {
                await _removeFromWishlist(item['id']);
                _fetchWishlistItems();
              },
            ),
          );
        },
      ),
    );
  }

  Future<void> _removeFromWishlist(int id) async {
    final Database db = await openDatabase(
      join(await getDatabasesPath(), 'wishlist.db'),
    );

    await db.delete(
      'wishlist',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
