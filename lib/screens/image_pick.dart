import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

import '../ffi.dart';

class ImagePickPage extends StatelessWidget {
  const ImagePickPage({super.key, required this.spinePath});
  final String? spinePath;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: PickImage(spinePath: spinePath),
      ),
    );
  }
}

class PickImage extends StatefulWidget {
  const PickImage({super.key, required this.spinePath});
  final String? spinePath;

  @override
  PickImageState createState() => PickImageState();
}

class PickImageState extends State<PickImage> {
  String? scenePath;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: scenePath != null
                  ? Image.file(File(scenePath!), gaplessPlayback: true)
                  : Container()),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MaterialButton(
                onPressed: _onTakePhotoClick,
                child: const Text("Take a Photo"),
              ),
              const SizedBox(width: 16.0),
              MaterialButton(
                onPressed: _onSelectImageClick,
                child: const Text("Select Image"),
              ),
            ],
          ),
          if (scenePath != null && scenePath!.isNotEmpty)
            MaterialButton(
              color: Colors.blue,
              onPressed: _searchInImage,
              child: const Text("Show Images",
                  style: TextStyle(color: Colors.white)),
            ),
        ],
      ),
    );
  }

  void _searchInImage() {
    int result = -1;
    String toast = "Couldn't find book";
    if (widget.spinePath != null && scenePath != null) {
      result = featureFounder(widget.spinePath, scenePath);
    }

    if (-1 != result) {
      toast = "Found the book!";

      List<String> outputPath = scenePath!.split(".");
      outputPath[outputPath.length - 2] =
          "${outputPath[outputPath.length - 2]}1000";
      setState(() {
        scenePath = outputPath.join(".");
      });
    }
    Fluttertoast.showToast(
        msg: toast,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  @override
  void initState() {
    super.initState();
    askForPermissions();
  }

  Future<void> askForPermissions() async {
    // Request manageExternalStorage permission
    var manageStorageStatus = await Permission.manageExternalStorage.request();
    print("manageExternalStorage: $manageStorageStatus");

    // Request storage permission
    var storageStatus = await Permission.storage.request();
    print("storage: $storageStatus");
  }

  void _onTakePhotoClick() async {
    PaintingBinding.instance.imageCache.clear();
    final image = await ImagePicker()
        .pickImage(source: ImageSource.camera, imageQuality: 100);
    if (image == null) return;
    setState(() => scenePath = image.path);
  }

  void _onSelectImageClick() async {
    PaintingBinding.instance.imageCache.clear();
    final image = await ImagePicker()
        .pickImage(source: ImageSource.gallery, imageQuality: 100);
    if (image == null) return;
    setState(() => scenePath = image.path);
  }
}
