import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../blocs/book_details_dialog.dart';
import '../models/book_model.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: BookListScreen(),
    );
  }
}

class BookListScreen extends StatefulWidget {
  const BookListScreen({super.key});

  @override
  BookListScreenState createState() => BookListScreenState();
}

class BookListScreenState extends State<BookListScreen> {
  List<Book> allBooks = [];
  List<Book> filteredBooks = [];

  @override
  void initState() {
    super.initState();
    fetchTopBooks();
  }

  Future<void> fetchTopBooks() async {
    const url =
        'https://www.googleapis.com/books/v1/volumes?q=subject:fiction&maxResults=40&orderBy=relevance';
    const defaultImageUrl =
        "https://onlinebookclub.org/book-covers/id467907-125.jpg";
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final bookItems = data['items'] as List<dynamic>;

      final books = bookItems
          .map(
            (item) => Book(
              title: item['volumeInfo']['title'] ?? 'Unknown Title',
              authors: List<String>.from(item['volumeInfo']['authors'] ?? []),
              imageUrl: item['volumeInfo']['imageLinks']?['thumbnail'] ??
                  defaultImageUrl,
              rating: item['volumeInfo']['averageRating']?.toDouble() ?? 0.0,
              description: item['volumeInfo']['description'] ?? '',
            ),
          )
          .toList();

      setState(() {
        allBooks = books;
        filteredBooks = books;
      });
    }
  }

  Future<void> searchBooks(String query) async {
    final url =
        'https://www.googleapis.com/books/v1/volumes?q=$query&maxResults=20&orderBy=relevance';
    final response = await http.get(Uri.parse(url));
    const defaultImageUrl =
        "https://onlinebookclub.org/book-covers/id467907-125.jpg";

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final bookItems = data['items'] as List<dynamic>;

      final books = bookItems
          .map(
            (item) => Book(
              title: item['volumeInfo']['title'] ?? 'Unknown Title',
              authors: List<String>.from(item['volumeInfo']['authors'] ?? []),
              imageUrl: item['volumeInfo']['imageLinks']?['thumbnail'] ??
                  defaultImageUrl,
              rating: item['volumeInfo']['averageRating']?.toDouble() ?? 0.0,
              description: item['volumeInfo']['description'] ?? '',
            ),
          )
          .toList();

      setState(() {
        filteredBooks = books;
      });
    }
  }

  void filterBooks(String query) {
    final lowerCaseQuery = query.toLowerCase();
    final filtered = allBooks.where((book) {
      final titleLower = book.title.toLowerCase();
      final authorLower = book.authors.join(' ').toLowerCase();
      return titleLower.contains(lowerCaseQuery) ||
          authorLower.contains(lowerCaseQuery);
    }).toList();

    setState(() {
      filteredBooks = filtered;
    });
  }

  Widget buildBookGridTile(Book book) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return BookDetailsDialog(book: book);
          },
        );
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: Colors.black12,
              offset: Offset(0, 2),
              blurRadius: 6,
            ),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: GridTile(
            footer: GridTileBar(
              backgroundColor: Colors.black45,
              title: Text(
                book.title,
                textAlign: TextAlign.center,
              ),
              subtitle: Text(
                book.authors.join(', '),
                textAlign: TextAlign.center,
              ),
            ),
            child: Image.network(
              book.imageUrl,
              fit: BoxFit.cover,
              errorBuilder: (BuildContext context, Object exception,
                  StackTrace? stackTrace) {
                // Display alternative content when the image fails to load
                return Container(
                  color: Colors.grey[300],
                  child: Center(
                    child: Icon(
                      Icons.broken_image,
                      color: Colors.grey[600],
                      size: 64,
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: SizedBox(
              height: 40,
              child: TextField(
                onChanged: (value) {
                  searchBooks(value);
                },
                decoration: const InputDecoration(
                  labelText: 'Search Book',
                  prefixIcon: Icon(Icons.search),
                  contentPadding: EdgeInsets.zero,
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16.0), // Add outer padding
              child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 16,
                  mainAxisSpacing: 16,
                  childAspectRatio: 0.5,
                ),
                itemCount: filteredBooks.length,
                itemBuilder: (context, index) {
                  final book = filteredBooks[index];
                  return buildBookGridTile(book);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
