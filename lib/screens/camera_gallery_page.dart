import 'dart:io';

import 'package:bookshelf/ffi.dart';
import 'package:bookshelf/screens/library_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

import '../blocs/image_paths_cubit.dart';

class CameraGalleryPage extends StatelessWidget {
  const CameraGalleryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
        body: SafeArea(
      child: PickFile(),
    ));
  }
}

class PickFile extends StatefulWidget {
  const PickFile({super.key});

  @override
  PickFileState createState() => PickFileState();
}

class PickFileState extends State<PickFile> {
  String? imagePath;
  int processMillisecond = 0;
  List<String>? imagePaths;
  bool search = false;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: imagePath != null
                  ? Image.file(File(imagePath!), gaplessPlayback: true)
                  : Container()),
          Text(processMillisecond > 0
              ? "Process Millisecond: $processMillisecond"
              : ""),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MaterialButton(
                onPressed: _onTakePhotoClick,
                child: const Text("Take a Photo"),
              ),
              const SizedBox(width: 16.0),
              MaterialButton(
                onPressed: _onSelectImageClick,
                child: const Text("Select Image"),
              ),
            ],
          ),
          MaterialButton(
            color: Colors.black,
            onPressed: _onConvertClick,
            child: const Text("Detect Spines",
                style: TextStyle(color: Colors.white)),
          )
        ],
      ),
    );
  }

  void _showImageListDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return const LibraryPage();
      },
    );
  }

  @override
  void initState() {
    super.initState();
    askForPermissions();
  }

  Future<void> askForPermissions() async {
    // Request manageExternalStorage permission
    var manageStorageStatus = await Permission.manageExternalStorage.request();
    print("manageExternalStorage: $manageStorageStatus");

    // Request storage permission
    var storageStatus = await Permission.storage.request();
    print("storage: $storageStatus");
  }

  void _onConvertClick() async {
    if (imagePath != null) {
      List<String> outputPath = imagePath!.split(".");
      outputPath[outputPath.length - 2] =
          "${outputPath[outputPath.length - 2]}_processed";
      Stopwatch stopwatch = Stopwatch()..start();
      int numberOfImages = drawSpineLines(imagePath!, outputPath.join("."));
      processMillisecond = stopwatch.elapsedMilliseconds;
      stopwatch.stop();
      if (numberOfImages >= 1) {
        search = true;
        imagePaths = List.generate(numberOfImages, (i) {
          String fileName =
              outputPath.sublist(0, outputPath.length - 1).join(".");
          String extension = outputPath.last;
          print("$fileName$i.$extension");
          return "$fileName$i.$extension";
        });
        context.read<ImagePathsCubit>().setImagePaths(imagePaths);
      }
      setState(() {
        imagePath = outputPath.join(".");
      });
    }
  }

  void _onTakePhotoClick() async {
    search = false;
    imagePaths?.clear();
    PaintingBinding.instance.imageCache.clear();
    final image = await ImagePicker()
        .pickImage(source: ImageSource.camera, imageQuality: 100);
    if (image == null) return;
    setState(() => imagePath = image.path);
  }

  void _onSelectImageClick() async {
    search = false;
    imagePaths?.clear();
    PaintingBinding.instance.imageCache.clear();
    final image = await ImagePicker()
        .pickImage(source: ImageSource.gallery, imageQuality: 100);
    if (image == null) return;
    setState(() => imagePath = image.path);
  }
}
