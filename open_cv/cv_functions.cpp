#include <cmath>
#include <cstring>
#include <iostream>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <sstream>
#include <stdio.h>
#include <vector>

#ifdef __ANDROID__
#include <android/log.h>
#endif

using namespace std;
using namespace cv;

extern "C" {
void platform_log(const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
#ifdef __ANDROID__
  __android_log_vprint(ANDROID_LOG_VERBOSE, "FFI Logger: ", fmt, args);
#else
  vprintf(fmt, args);
#endif
  va_end(args);
}

bool loadImage(const string &filename, Mat &img) {
  img = imread(filename);
  if (img.empty()) {
    cout << "Could not open or find the image: " << filename << endl;
    return false;
  }
  return true;
}

void detectKeypointsAndDescriptors(const Mat &img, vector<KeyPoint> &keypoints,
                                   Mat &descriptors) {
  Ptr<SIFT> detector = SIFT::create();
  detector->detectAndCompute(img, noArray(), keypoints, descriptors);
}

void matchDescriptors(const Mat &descriptors_object,
                      const Mat &descriptors_scene,
                      vector<DMatch> &good_matches) {
  Ptr<DescriptorMatcher> matcher =
      DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);
  vector<vector<DMatch>> knn_matches;
  matcher->knnMatch(descriptors_object, descriptors_scene, knn_matches, 2);

  const float ratio_thresh = 0.75f;
  for (size_t i = 0; i < knn_matches.size(); i++) {
    if (knn_matches[i][0].distance <
        ratio_thresh * knn_matches[i][1].distance) {
      good_matches.push_back(knn_matches[i][0]);
    }
  }
}

bool findHomographyMatrix(const vector<KeyPoint> &keypoints_object,
                          const vector<KeyPoint> &keypoints_scene,
                          const vector<DMatch> &good_matches, Mat &H) {
  vector<Point2f> obj;
  vector<Point2f> scene;
  for (size_t i = 0; i < good_matches.size(); i++) {
    obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
    scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
  }

  H = findHomography(obj, scene, RANSAC);
  if (H.empty()) {
    cerr << "Failed to find homography!" << endl;
    return false;
  }
  return true;
}

void drawBoundingBox(Mat &img, const Mat &H, const Mat &img_object) {
  vector<Point2f> obj_corners(4);
  obj_corners[0] = Point2f(0, 0);
  obj_corners[1] = Point2f(static_cast<float>(img_object.cols), 0);
  obj_corners[2] = Point2f(static_cast<float>(img_object.cols),
                           static_cast<float>(img_object.rows));
  obj_corners[3] = Point2f(0, static_cast<float>(img_object.rows));

  vector<Point2f> scene_corners(4);
  perspectiveTransform(obj_corners, scene_corners, H);

  line(img, scene_corners[0], scene_corners[1], Scalar(0, 255, 0), 4);
  line(img, scene_corners[1], scene_corners[2], Scalar(0, 255, 0), 4);
  line(img, scene_corners[2], scene_corners[3], Scalar(0, 255, 0), 4);
  line(img, scene_corners[3], scene_corners[0], Scalar(0, 255, 0), 4);
}

void resize_img(const Mat &img, Mat &resized_image) {
  img.copyTo(resized_image);
  int img_ht = img.rows;
  int img_wd = img.cols;
  double ratio = static_cast<double>(img_wd) / static_cast<double>(img_ht);
  int new_width = 500;
  int new_height = ceil(static_cast<double>(new_width) / ratio);
  resize(img, resized_image, Size(new_width, new_height));
}

void merge_lines(vector<Vec4i> &lines, double threshold, int height,
                 vector<Vec4i> &unique_lines) {
  std::sort(lines.begin(), lines.end(),
            [](const cv::Vec4i &line1, const cv::Vec4i &line2) {
              return line1[0] < line2[0];
            });

  unique_lines.clear();

  for (Vec4i line : lines) {
    if (line[1] < line[3]) {
      line[1] = 0;
      line[3] = height;
    } else {
      line[1] = height;
      line[3] = 0;
    }

    if (unique_lines.empty()) {
      unique_lines.push_back(line);
    } else {
      bool isUnique = true;

      for (Vec4i checked_line : unique_lines) {
        double distance = sqrt(pow(line[0] - checked_line[0], 2) +
                               pow(line[1] - checked_line[1], 2) +
                               pow(line[2] - checked_line[2], 2) +
                               pow(line[3] - checked_line[3], 2));
        if (distance <= threshold) {
          isUnique = false;
          break;
        }
      }

      if (isUnique) {
        unique_lines.push_back(line);
      }
    }
  }
}

void detectBookSpines(cv::Mat &origin_image, cv::Mat &image, int angleThreshold,
                      std::vector<cv::Mat> &output_spines) {

  cv::Mat gray;
  cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);

  int minRowHeight = static_cast<int>(image.rows * 20 / 100);

  cv::Mat blur;
  cv::GaussianBlur(gray, blur, cv::Size(5, 5), 0);

  cv::Mat edges;
  cv::Canny(blur, edges, 50, 150, 3);

  std::vector<cv::Vec4i> lines;
  cv::HoughLinesP(edges, lines, 1, CV_PI / 180, 100, 70, 10);

  if (lines.empty()) {
    lines = {{0, 0, image.cols, 0}, {0, image.rows, image.cols, image.rows}};
  } else {
    lines.insert(lines.begin(), {0, 0, image.cols, 0});
    lines.push_back({0, image.rows, image.cols, image.rows});
  }

  std::vector<cv::Vec4i> horizontalLines;
  for (const auto &line : lines) {
    if (std::abs(line[3] - line[1]) < angleThreshold) {
      horizontalLines.push_back(line);
    }
  }

  std::sort(horizontalLines.begin(), horizontalLines.end(),
            [](const cv::Vec4i &line1, const cv::Vec4i &line2) {
              return line1[1] < line2[1];
            });

  if (horizontalLines.empty()) {
    horizontalLines = {{0, 0, image.cols, 0},
                       {0, image.rows, image.cols, image.rows}};
  }

  double scale_x = static_cast<double>(origin_image.cols) / image.cols;
  double scale_y = static_cast<double>(origin_image.rows) / image.rows;

  for (size_t i = 0; i < horizontalLines.size() - 1; i++) {
    int rowHeight = horizontalLines[i + 1][1] - horizontalLines[i][1];
    if (rowHeight < minRowHeight) {
      continue;
    }

    cv::Mat row =
        image(cv::Range(horizontalLines[i][1], horizontalLines[i + 1][1]),
              cv::Range(0, image.cols));

    cv::Mat rowCopy = row.clone();
    cv::Mat Hrow = origin_image(cv::Range(horizontalLines[i][1] * scale_x,
                                          horizontalLines[i + 1][1] * scale_x),
                                cv::Range(0, origin_image.cols));

    cv::Mat grayRow;
    cv::cvtColor(row, grayRow, cv::COLOR_BGR2GRAY);

    cv::Mat blurred;
    cv::GaussianBlur(grayRow, blurred, Size(3, 3), 0);

    cv::Mat adaptiveThresh;
    cv::adaptiveThreshold(blurred, adaptiveThresh, 255,
                          cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV,
                          11, 2);

    cv::Mat vertical_kernel =
        cv::getStructuringElement(cv::MORPH_RECT, cv::Size(1, 5));

    cv::Mat erode;
    cv::erode(adaptiveThresh, erode, vertical_kernel, cv::Point(-1, -1), 8);

    cv::Mat sobelX;
    cv::Sobel(erode, sobelX, CV_64F, 1, 0, 3);

    cv::convertScaleAbs(sobelX, sobelX);

    cv::Mat sobelThresh;
    cv::threshold(sobelX, sobelThresh, 50, 255, cv::THRESH_BINARY);

    std::vector<cv::Vec4i> linesRow;
    cv::HoughLinesP(sobelThresh, linesRow, 1, CV_PI / 180,
                    static_cast<int>(sobelThresh.cols * 0.1),
                    sobelThresh.rows * 0.1);

    if (!linesRow.empty()) {
      vector<cv::Vec4i> uniques;
      merge_lines(linesRow, static_cast<int>(0.04 * sobelThresh.cols),
                  sobelThresh.rows, uniques);
      int spineStart = 0;
      int spineEnd = 0;
      int highStart = 0;
      int highEnd = 0;
      int spineCount = 0;

      for (const auto &line : uniques) {
        int x1 = line[0], y1 = line[1], x2 = line[2], y2 = line[3];
        double angle =
            (x2 - x1 == 0)
                ? 90
                : std::abs(std::atan((y2 - y1) / (x2 - x1))) * 180 / CV_PI;

        if (angle >= 90 - angleThreshold && angle <= 90 + angleThreshold) {
          cv::line(image, cv::Point(x1, y1 + horizontalLines[i][1]),
                   cv::Point(x2, y2 + horizontalLines[i][1]),
                   cv::Scalar(0, 0, 255), 2);

          cv::line(rowCopy, cv::Point(x1, y1), cv::Point(x2, y2),
                   cv::Scalar(0, 0, 255), 2);
          spineEnd = x1;
          highEnd = x1 * scale_x;

          if (spineStart > 0 && spineEnd <= row.cols &&
              spineStart <= spineEnd) {
            cv::Mat spineImage =
                row(cv::Range::all(), cv::Range(spineStart, spineEnd));

            cv::Mat highSpine =
                Hrow(cv::Range::all(), cv::Range(highStart, highEnd));

            if (spineImage.cols > 5) {
              output_spines.push_back(highSpine);
            }
          }
          spineStart = x2;
          highStart = x2 * scale_x;
          spineCount++;
        }
      }
    }
  }
}

void resizeImage(const cv::Mat &image, cv::Mat &resizedImage) {
  double ratio = static_cast<double>(image.cols) / image.rows;
  int newWidth = 500;
  int newHeight = static_cast<int>(newWidth / ratio);
  cv::resize(image, resizedImage, cv::Size(newWidth, newHeight));
}

char *concatNumber(const char *outputPath, int number) {
  std::string path(outputPath);
  size_t dotPos = path.find_last_of('.');
  if (dotPos != std::string::npos) {
    std::ostringstream oss;
    oss << number;
    std::string numberString = oss.str();

    path.insert(dotPos, numberString);

    char *updatedPath = new char[path.length() + 1];
    std::strcpy(updatedPath, path.c_str());

    return updatedPath;
  } else {
    return nullptr;
  }
}

__attribute__((visibility("default"))) __attribute__((used)) int
feature_finder(char *object, char *scene, char *outputPath) {
  Mat img_object, img_scene;
  if (!loadImage(object, img_object) || !loadImage(scene, img_scene)) {
    return -1;
  }

  Mat gray_object, gray_scene;
  cvtColor(img_object, gray_object, COLOR_BGR2GRAY);
  cvtColor(img_scene, gray_scene, COLOR_BGR2GRAY);

  Mat gray_o, gray_s;
  resize_img(gray_object, gray_o);
  resize_img(gray_scene, gray_s);

  vector<KeyPoint> keypoints_object, keypoints_scene;
  Mat descriptors_object, descriptors_scene;
  detectKeypointsAndDescriptors(gray_o, keypoints_object, descriptors_object);
  detectKeypointsAndDescriptors(gray_s, keypoints_scene, descriptors_scene);

  vector<DMatch> good_matches;
  matchDescriptors(descriptors_object, descriptors_scene, good_matches);

  if (good_matches.empty()) {
    return -1;
  }

  Mat H;
  if (!findHomographyMatrix(keypoints_object, keypoints_scene, good_matches,
                            H)) {
    return -1;
  }

  Mat img_s, img_o;
  resize_img(img_object, img_o);
  resize_img(img_scene, img_s);

  drawBoundingBox(img_s, H, img_o);

  char *new_path = concatNumber(outputPath, 1000);
  imwrite(new_path, img_s);
  platform_log("Output Path: %s", new_path);
  delete[] new_path;

  return 0;
}

__attribute__((visibility("default"))) __attribute__((used)) int
draw_spine_lines(char *image_path, char *outputPath) {
  Mat img = imread(image_path);
  Mat final_image;
  resize_img(img, final_image);
  vector<Mat> spines;
  detectBookSpines(img, final_image, 20, spines);

  platform_log("input path: %s\n", outputPath);

  imwrite(outputPath, final_image);
  int i = 0;
  for (const auto &spine : spines) {
    char *new_path = concatNumber(outputPath, i++);
    imwrite(new_path, spine);
    platform_log("Output Path: %s", new_path);
    delete[] new_path;
  }

  return i;
}
}
